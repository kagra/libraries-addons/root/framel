#pragma once

// Frame library dependencies
#include <FrameL.h>

// ROOT+ library dependencies
#include <TClock.h>
#include <TVarexp.h>
#include <TPicasso.h>

// thread dependencies
#include <thread>

// abi:cxx dependency
#include <cxxabi.h>

struct FrTrendData {
  char *name;      /* Channel name */
  FrAdcData *inadc;    /* Input ADC data */
  FrAdcData *nADC, *minADC, *maxADC, *meanADC, *rmsADC;
  int n;
  float min;
  float max;
  double sum;
  double sumsq;
  struct FrTrendData *next;
};

inline FrameH *FrNewFrame(TString name, double GPStime, double frameStep) {

        FrameH *frame = FrameHNew(const_cast<char*>(name.Data()));
        if (frame == NULL) {
                fprintf( stderr, "*** Cannot create output frame\n" );
                return NULL;
        }

        frame->run = 0;
        frame->frame = 0;
        frame->dataQuality = 0;
        frame->GTimeS = gClockGPS->TimeS(GPStime);
        frame->GTimeN = gClockGPS->TimeN(GPStime);
        frame->ULeapS = gClockGPS->GetLeapSeconds(GPStime);
        frame->dt = (double) frameStep;

        return frame;
}

inline FrTrendData *FrTrend(FrameH *)
{
        return new FrTrendData();
}

inline void FrTrendFree(FrTrendData *trendData)
{

}

// CHAR   = char           = 1 byte
// CHAR_U = unsigned char  = 1 byte
// INT_2S = short          = 2 bytes
// INT_2U = unsigned short = 2 bytes
// INT_4S = int            = 4 bytes
// INT_4U = unsigned int   = 4 bytes
// INT_8S = long          = 8 bytes
// INT_8U = unsigned long = 8 bytes
// REAL_4 = float         = 4 bytes
// REAL_8 = double        = 8 bytes
// STRING = char[]          = < 65536 bytes
// DOUBLE = double          = 6 bytes
// COMPLEX_8  = <float,float>   = 8 bytes
// COMPLEX_16 = <double,double> = 16 bytes

template<typename T>
std::vector<T> FrCast(std::vector<char> c)
{
        std::vector<T> v(c.size()/sizeof(T));
        for(int i = 0, N = v.size(); i < N; i++)
                memcpy(&v[i], &c[i*sizeof(T)], std::min(c.size(), sizeof(T)));

        return v;
}

template <typename T>
std::vector<T> FrCast(FrVect *vect)
{
        std::vector<char> bytes = std::vector<char>(vect->data, vect->data + vect->nBytes);
        switch(vect->type) {

                case FR_VECT_C:
                {
                        std::vector<char> v = FrCast<char>(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_2S:
                { 
                        std::vector<short> v = FrCast<short>(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_4R:
                { 
                        std::vector<float> v = FrCast<float>(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_8R:
                { 
                        std::vector<double> v = FrCast<double>(bytes);  
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_4S:
                { 
                        std::vector<int> v = FrCast<int>(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_8S:
                { 
                        std::vector<long> v = FrCast<long>(bytes);                              
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_STRING:
                { 
                        std::vector<char> v = FrCast<char>(bytes); 
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_2U:
                { 
                        std::vector<unsigned short> v = FrCast<unsigned short>(bytes); 
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_4U:
                { 
                        std::vector<unsigned int> v = FrCast<unsigned int  >(bytes); 
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_8U:
                { 
                        std::vector<unsigned long> v = FrCast<unsigned long >(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_1U:
                { 
                        std::vector<unsigned char> v = FrCast<unsigned char >(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_8C:
                { 
                        std::vector<float> v = FrCast<float>(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
                case FR_VECT_16C:
                {  
                        std::vector<double> v = FrCast<double>(bytes);
                        return std::vector<T>(v.begin(), v.end());
                }
        }  

        return {};       
}

inline FrEndOfFile *FrEndOfFileReadAndReturn(FrFile *iFile)
{
        unsigned int nFrames, chkSumFlag, chkSum, nBytes4, offset4,
        chkSumFrHeader, chkSumFile;

        FRLONG offset, nBytesEnd;

        /*--------open a real file or set pointers for in-memory read if needed---*/
        if(iFile->frfd == NULL && iFile->buf == NULL) FrFileIOpen(iFile);
        if(iFile->error != FR_OK) return(NULL);

        if(iFile->inMemory == FR_NO && iFile->frfd == NULL) return(NULL);

                        /*--do not compute file checksum when using random access ---*/
 
        iFile->chkSumFiFlag = FR_NO;

        /*---------------- read the FrEndOfFile data for differnet format version---*/

        if(iFile->fmtVersion < 6) {

                if(FrFileIOSetFromEnd(iFile, -20) == -1) {return(NULL);}
                FrReadIntU(iFile, &nFrames);
                FrReadIntU(iFile, &nBytes4);
                iFile->nBytesE = nBytes4;
                FrReadIntU(iFile, &chkSumFlag);
                FrReadIntU(iFile, &chkSum);
                FrReadIntU(iFile, &offset4);
                offset = offset4;
      
        }    /*--- skip the record length(INT4) and type(INT2) */

        else if(iFile->fmtVersion < 8) {
                
                if(FrFileIOSetFromEnd(iFile, -28) == -1) {return(NULL);}
                FrReadIntU(iFile, &nFrames);
                FrReadLong(iFile, (FRLONG*) &iFile->nBytesE);
                FrReadIntU(iFile, &chkSumFlag);
                FrReadIntU(iFile, &chkSum);
                FrReadLong(iFile, &offset);
        
        } else {

                if(FrFileIOSetFromEnd(iFile, -32) == -1) {return(NULL);}
                nBytesEnd = FrFileIOTell(iFile)+32;
                if(nBytesEnd == FRIOBSIZE) nBytesEnd = iFile->frfd->nBytesRead;
                FrReadIntU(iFile, &nFrames);
                FrReadLong(iFile, (FRLONG*) &iFile->nBytesE);
                if(nBytesEnd != iFile->nBytesE) {return(NULL);}
                FrReadLong(iFile, &offset);
                FrReadIntU(iFile, &chkSumFrHeader);
                FrReadIntU(iFile, &chkSum);
                FrReadIntU(iFile, &chkSumFile);
                chkSumFlag = 0;
        }

        FrEndOfFile *eof = new FrEndOfFile();
        eof->classe = FrEndOfFrameDef();
        eof->nFrames = nFrames;
        eof->nBytes = iFile->nBytesE;
        eof->seekTOC = offset;

        /*---------------------------------------------- check the file checksum---*/

        if(iFile->chkTypeFiRead == 0) {}
        else if(iFile->chkSumFiFlag == FR_NO) {}
        else if(iFile->chkSumFiRead  == iFile->chkSumFi) {}
        else {
        iFile->error = FR_ERROR_CHECKSUM;
        return eof;}

        eof->chkType = iFile->chkTypeFiRead;
        eof->chkSum = iFile->chkSumFi;
        return eof;
}

inline void FrLibSetLvlT(int level) { FrLibSetLvl(level); }

// This is a partial reading of the frame mainly for rawData reading
// NB: Implementation for other variables should be verified and/or implemented (detector, auxiliary data, ..)
inline FrameH *FrameReadT(FrFile *iFile, double gtime, const char *tag)
{
        FrLibSetLvl(0);
        
        /** Prepare an FrameH with minimal information **/
        FrameH *frame = FrameHReadT(iFile,  gtime);
        if(frame == NULL) return(NULL);
        if(strlen(tag) == 0) return frame;

        FrVect *snr, *gps;
        int nData, i, index;
        FrTag* word, *listOfWord;
        char star[] = "*";
        char *name;
        double sampleRate;

        /** ADC Channel with specific tag only */
        FrRawDataNew(frame);
        if(frame->rawData != NULL) {
                frame->rawData->firstSer = FrSerDataReadT(iFile, const_cast<char *>(tag), gtime);
                frame->rawData->firstAdc = FrAdcDataReadT(iFile, const_cast<char *>(tag), gtime);
        }
        
        frame->procData = FrProcDataReadT(iFile, const_cast<char *>(tag), gtime);    
        frame->simData  = FrSimDataReadT (iFile, const_cast<char *>(tag), gtime);

        if(strstr(const_cast<char *>(tag),"EVENT_SNR:") != 0) {
                listOfWord = FrTagNew(const_cast<char *>(tag));

                for(word = listOfWord; word != NULL; word = word->next)
                {if(strncmp(word->start,"EVENT_SNR:",10) == 0) 
                {name = word->start+10;
                        if(strncmp(name,"ALL",3) == 0) name = star;
                        gps = FrFileIGetEventInfo(iFile, name, gtime, frame->dt, 0., 1.e37);}
                else if(strncmp(word->start,"SIM_EVENT_SNR:",14) == 0) 
                {name = word->start+14;
                        if(strncmp(name,"ALL",3) == 0) name = star;
                        gps = FrFileIGetSimEventInfo(iFile, name, gtime, frame->dt, 0, 1.e37);}
                else {continue;}

                sampleRate = 1000.;
                nData = frame->dt*sampleRate + 0.1;
                snr = FrVectNewTS(word->start, sampleRate, nData, -32);
                if(snr == NULL) return(frame);
                snr->GTime = gtime;

                if(gps != NULL)
                {for(i=0; i<gps->nData; i++)
                {index = FrVectGetIndex(snr, gps->dataD[i]-gtime);
                snr->dataF[index] += gps->next->dataF[i];}}

                FrProcDataNewV(frame, snr);
                FrVectFree(gps);}

                FrTagFree(listOfWord);
        }

        return(frame);
}

inline FrEndOfFrame *FrEndOfFrameReadAndReturn(FrFile *iFile)
/*---------------------------------------------------------------------------*/
{
        unsigned int run, frameNumber, instance, gTimeS, gTimeN, chkSumFlag, zero = 0;
        FRULONG nBytes;
        unsigned short instance2;

        if(iFile->fmtVersion == 4) {

                FrBack4EndOfFrameRead(iFile);
                if(iFile->error != FR_OK) return NULL;

                FrEndOfFrame *eof = new FrEndOfFrame();
                              eof->classe  = FrEndOfFrameDef();
                              eof->run     = iFile->run;
                              eof->frame   = iFile->curFrame->frame;
                              eof->chkType = 0;
                              eof->chkSum  = 0;

                return eof;
        }

        if(iFile->fmtVersion > 5) FrReadIntU  (iFile, &instance);
        else FrReadShortU(iFile, &instance2);

        FrReadIntU(iFile, &run);
        FrReadIntU(iFile, &frameNumber);

        FrEndOfFrame *eof = new FrEndOfFrame();
                      eof->classe  = FrEndOfFrameDef();
                      eof->run     = run;
                      eof->frame   = frameNumber;

        /*--------- checksum test for version lower than 8 ---*/
        if(iFile->fmtVersion < 8)
        {
                FrReadIntU(iFile, &iFile->chkTypeFrRead);

                chkSumFlag = iFile->chkSumFrFlag;
                if(iFile->chkTypeFrRead == 1) { /*-- chkSum is set to 0 when computing it--*/
                iFile->chkSumFrFlag = FR_NO;
                FrCksumGnu((char *) &zero, sizeof(int), &(iFile->chkSumFr));}
                FrReadIntU(iFile, &iFile->chkSumFrRead);
                iFile->chkSumFrFlag = chkSumFlag ? FR_YES : FR_NO;

                nBytes = iFile->nBytes - iFile->nBytesF;

                /*--------- check checksum ----------------------*/
                if((iFile->chkTypeFrRead == 1) && (iFile->chkSumFrFlag == FR_YES))
                {

                        FrCksumGnu( NULL, nBytes, &(iFile->chkSumFr));
                        if(iFile->chkSumFrRead  != iFile->chkSumFr) {
                                iFile->error = FR_ERROR_CHECKSUM;
                                return eof;
                        }

                        eof->chkType = iFile->chkTypeFrRead;
                        eof->chkSum  = iFile->chkSumFrRead;
                }

                /*-------------------- checksum for frame format v8 and above ---*/
                else {
                        FrReadIntU(iFile, &gTimeS);
                        FrReadIntU(iFile, &gTimeN);
                        FrReadStructChksum(iFile);
                }

                eof->chkType = iFile->chkTypeFrRead;
                eof->chkSum  = iFile->chkSumFrRead;
                iFile->endOfFrame = FR_YES;
        }
 else {
   FrReadIntU(iFile, &gTimeS);
   FrReadIntU(iFile, &gTimeN);
   FrReadStructChksum(iFile);}

 if(iFile->curFrame != NULL) {

   {if(run != iFile->curFrame->run)
     {FrError(3, (char*) "FrEndOfFrameRead", (char*) "run number missmatch");
      iFile->error = FR_ERROR_BAD_END_OF_FRAME;
      return eof;}

    if(frameNumber != iFile->curFrame->frame)
     {FrError(3, (char*) "FrEndOfFrameRead", (char*) "frame number missmatch");
      iFile->error = FR_ERROR_BAD_END_OF_FRAME;
      return eof;}}
 }
 iFile->endOfFrame = FR_YES;


        return eof;
}
