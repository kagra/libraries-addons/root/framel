#pragma once

#include <Rioplus.h>
#include <TObject.h>
#include <TString.h>
#include <TTree.h>
#include <TFile.h>

#include "TFrameTypes.h"
#include "TPrint.h"

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

namespace FrameFormat {

    enum Verbosity {NORMAL = 0, INFO = 1, DEBUG = 3, ALL = 5};

    class TFrameChain;
    class TFrameAbstract: public TObject
    {
        protected: 
            static Verbosity verbosity;
            
        public:

            // TFrame verbosity control
            Verbosity GetVerbosity();
            void SetVerbosity(Verbosity verbosity);
            bool Verbose(Verbosity verbosity);

            // FrameL debug control 
            static void SetDebug(int debugLvl);

            // FrameL version
            static char *Version();
            
        ClassDef(TFrameAbstract, 1);
    };
};
