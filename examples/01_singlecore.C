#include <TFrameFormat.h>

int main()
{
    FrameFormat::TFrameChain f("${PWD}/dataset/test-1.gwf ${PWD}/dataset/test-2.gwf", "d", "fast*");
    f.SetVerbosity(FrameFormat::Verbosity::DEBUG);

    for(int i = 0, I = f.GetNFiles(); i < I; i++) {

        std::cout << std::endl;
        f.Print("", i);   

        for(int j = 0, J = f.GetNFrames(i); j < J; j++) {
        
            FrameFormat::TFrame *frame = f.GetFrame(i, j);
            frame->Print();

            std::vector<FrameFormat::FrType> types = frame->ReadTypes();
            if(std::find(types.begin(), types.end(), FrameFormat::FrType::FrAdcData) != types.end()) {

                std::vector<FrameFormat::TFrAdcData*> v = frame->Read<FrameFormat::TFrAdcData>();
                for(int m = 0, M = v.size(); m < M; m++) {

                    FrameFormat::TFrAdcData *adcData = v[m];
                    std::cout << "             >> ADC Channel: " << adcData->name << "` " << std::endl;

                    TObject *obj = f.GetObject(adcData->name);
                    if(obj) std::cout << "                ROOT Object(TTree*) = " << obj << std::endl;
                }

                std::cout << std::endl;
            }
        }
    }

    return 0;
}
